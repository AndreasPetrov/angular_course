import { Component, OnInit, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [],
  exports: [RouterModule]
})
@Component({
  selector: 'app-launcher',
  templateUrl: './launcher.component.html',
  styleUrls: ['./launcher.component.scss']
})

export class LauncherComponent implements OnInit {

  constructor() { }
  currentValue = 0.0;
  resultShort: Array<number>;
  resultLong: Array<number>;
  ngOnInit() {
    console.log('ngOnInit called');
  }

  *generator(inputArray: Array<number>, index: number) {
    yield inputArray[index];
  }

  *merge(inputArray1: Array<number>, inputArray2: Array<number>) {
    // for (let index = 0; index < inputArray1.length; index++) {
    //     yield this.generator(inputArray1, index).next(index);
    // }
    yield inputArray1.concat(inputArray2).sort((n1, n2) => n1 - n2);
    // for (let index = 0; index < inputArray2.length; index++) {
    //   yield this.generator(inputArray2, index).next(index);
    // }
  }

  getMergedArray(inputArray: Array<number>[]) {
  //   return inputArray.merge((summ, currval) => {
  //     return summ.concat(currval);
  // });
    return  inputArray.reduce((summ, currval) => {
        return summ.concat(currval);
    });
  }

  callMergeArraysShort() {
    const inputArray = [[1, 7, 11, 17], [3, 5, 13]];
    this.resultShort = this.getMergedArray(inputArray)
                      .sort((n1, n2) => n1 - n2);
    const result = this.merge([3, 5, 13],  [1, 7, 11, 17]).next(1).value;
    console.log(result);
    // console.log(this.generator(10).next(2));
  }

  callMergeArraysLong() {
    const inputArray = [[2, 3, 5, 7, 11], [2, 4, 6, 8, 10, 12, 14]];
    this.resultLong = this.getMergedArray(inputArray)
                      .sort((n1, n2) => n1 - n2);
    console.log(this.resultLong);
  }
}
