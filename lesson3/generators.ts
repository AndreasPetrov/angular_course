    function* merge(inputArray1, inputArray2) {
        yield inputArray1.concat(inputArray2).sort((n1, n2) => n1 - n2);
    }
    
    function getMergedArray(inputArray) {
        return  inputArray.reduce((summ, currval) => {
            return summ.concat(currval);
        });
    }
    
    function callMergeArraysShort() {
        // const inputArray = [[1, 7, 11, 17], [3, 5, 13]];
        // this.resultShort = this.getMergedArray(inputArray)
        //                   .sort((n1, n2) => n1 - n2);
        this.resultShort = this.merge([1, 7, 11, 17],  [3, 5, 13]).next(1).value;
      
        console.log(this.resultShort);
    }
    
    function  callMergeArraysLong() {
        // const inputArray = [[2, 3, 5, 7, 11], [2, 4, 6, 8, 10, 12, 14]];
        // this.resultLong = this.getMergedArray(inputArray)
        //                   .sort((n1, n2) => n1 - n2);
        this.resultLong = this.merge([2, 3, 5, 7, 11],  [2, 4, 6, 8, 10, 12, 14]).next(1).value;
      
        console.log(this.resultLong);
    }

    function simpleMerge() {
        let result = callMergeArraysShort();
        console.log(result);
    }

    callMergeArraysLong();