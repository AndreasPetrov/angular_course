import { Component } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { fromPromise, ajaxGetJSON, ajax } from 'rxjs/internal-compatibility';
import {  map, merge, startWith, combineLatest, flatMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'angularObservable';
  refreshButton = document.querySelector('.refresh');
  closeButton1 = document.querySelector('.close1');
  closeButton2 = document.querySelector('.close2');
  closeButton3 = document.querySelector('.close3');
  refreshClickStream = fromEvent(this.refreshButton, 'click') as any;
  close1ClickStream = fromEvent(this.closeButton1, 'click');
  close2ClickStream = fromEvent(this.closeButton2, 'click');
  close3ClickStream = fromEvent(this.closeButton3, 'click');
  suggestion1Stream = this.createSuggestionStream(this.close1ClickStream);
  suggestion2Stream = this.createSuggestionStream(this.close2ClickStream);
  suggestion3Stream = this.createSuggestionStream(this.close3ClickStream);
  requestStream = this.refreshClickStream.
    pipe(
        map(() => {
          const randomOffset = Math.floor(Math.random() * 500);
          return 'https://api.github.com/users?since=' + randomOffset;
        }
      )
    );

  responseStream = this.requestStream.pipe(
      flatMap((requestUrl) => {
        console.log(requestUrl);
        return this.getProm(requestUrl);
    }));

  getProm(requestUrl: any) {
    return fromPromise(ajax.getJSON(requestUrl).toPromise());
  }

  constructor() {
    this.suggestion1Stream.subscribe((suggestedUser) => {
      this.renderSuggestion(suggestedUser, '.suggestion1');
    });

    this.suggestion2Stream.subscribe((suggestedUser) => {
      this.renderSuggestion(suggestedUser, '.suggestion2');
    });

    this.suggestion3Stream.subscribe((suggestedUser) => {
      this.renderSuggestion(suggestedUser, '.suggestion3');
    });
  }

  renderSuggestion(suggestedUser, selector) {
      // const suggestionEl = document.querySelector(selector);
      // if (suggestedUser === null) {
      //   suggestionEl.style.visibility = 'hidden';
      // } else {
      //   suggestionEl.style.visibility = 'visible';
      //   const usernameEl = suggestionEl.querySelector('.username');
      //   usernameEl.href = suggestedUser.html_url;
      //   usernameEl.textContent = suggestedUser.login;
      //   const imgEl = suggestionEl.querySelector('img');
      //   imgEl.src = '';
      //   imgEl.src = suggestedUser.avatar_url;
      // }
  }

  createSuggestionStream(closeClickStream: Observable<Event>) {
    console.log(closeClickStream);

    const combineLatestElem = closeClickStream.pipe(startWith('startup click'));

    console.log(combineLatestElem);

    const funcResult = (click, listUsers) => {
      return listUsers[Math.floor(Math.random() * listUsers.length)];
    };

    const result = combineLatestElem.pipe(combineLatest(this.responseStream, funcResult));

    return result.pipe(merge(this.refreshClickStream.pipe(map(() => {
                return null;
            }))
        ))
        .pipe(startWith(null));
  }

}
