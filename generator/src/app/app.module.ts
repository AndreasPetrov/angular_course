import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LauncherComponent } from './launcher/launcher.component';
import { InputElementsComponent } from './input-elements/input-elements.component';

@NgModule({
  declarations: [
    InputElementsComponent,
    LauncherComponent,
    AppComponent
  ],
  imports: [
    InputElementsComponent,
    LauncherComponent,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
