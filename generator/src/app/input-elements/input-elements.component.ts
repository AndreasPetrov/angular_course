import { Component, OnInit, NgModule  } from '@angular/core';
import { fromEvent } from 'rxjs';
import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { RouterModule } from '@angular/router';

const input = document.createElement('input');

@NgModule({
  imports: [],
  exports: [RouterModule]
})
 @Component({
  selector: 'app-input-elements',
  templateUrl: './input-elements.component.html',
  styleUrls: ['./input-elements.component.scss']
})

export class InputElementsComponent implements OnInit {

  constructor() { }
  ngOnInit() {
  range(1, 200)
        .pipe(filter(x => x % 2 === 1), map(x => x + x))
        .subscribe(x => console.log(x));
  }

}
