import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.scss']
})
export class MatrixComponent implements OnInit {


  constructor() { }


  matrix = [[ 11,  12,  13,  14,  15],
            [ 21,  22,  23,  24,  25],
            [ 31,  32,  33,  34,  35],
            [ 41,  42,  43,  44,  45],
            [ 51,  52,  53,  54,  55]];

  ngOnInit(): void {

  }

  rotateCounterClockMatrix(event) {
    const n = this.matrix.length - 1;
    for ( let i = 0; i < this.matrix.length / 2; i++) {
            for (let j = i; j < n - i; j++) {
                const tmp = this.matrix[i][j];
                this.matrix[i][j] = this.matrix[j][n - i];
                this.matrix[j][n - i] = this.matrix[n - i][n - j];
                this.matrix[n - i][n - j] = this.matrix[n - j][i];
                this.matrix[n - j][i] = tmp;
            }
      }
  }

  rotateClockwise(event) {
    const n = this.matrix.length - 1;
    for (let i = 0; i < this.matrix.length; i++) {
      for (let j = i; j < n - i; j++) {
            const tmp = this.matrix[i][j];
            this.matrix[i][j] = this.matrix[n - j][i];
            this.matrix[n - j][i] = this.matrix[n - i][n - j];
            this.matrix[n - i][n - j] = this.matrix[j][n - i];
            this.matrix[j][n - i] = tmp;
      }
    }
  }
}
