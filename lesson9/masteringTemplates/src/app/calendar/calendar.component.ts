import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  title = 'Calendar!';
  // tslint:disable-next-line:variable-name
  private _message;
  public get message() {
    return this._message;
  }
  public set message(value) {
    this._message = value;
  }
  // declared array of months.
  months = ['January', 'Feburary', 'March',
            'April', 'May', 'June',
            'July', 'August', 'September',
            'October', 'November', 'December'];
  isAvailable = true;
  selectedOldMonth = this.months[0];
  selectedNewMonth = '';

  constructor() { }

  ngOnInit() {
  }

  changemonths(event) {
    console.log(event);
    this.selectedNewMonth = event.target.value;
    console.log(event.target.value);
    this.message = 'Changed month from '
    + this.selectedOldMonth + ' to ' + this.selectedNewMonth  + ' in the Dropdown';
    console.log(this.message);
    this.selectedOldMonth = event.target.value;
 }

}
