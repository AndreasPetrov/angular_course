import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsideElementComponent } from './inside-element.component';

describe('InsideElementComponent', () => {
  let component: InsideElementComponent;
  let fixture: ComponentFixture<InsideElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsideElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsideElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
