import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inside-element',
  templateUrl: './inside-element.component.html',
  styleUrls: ['./inside-element.component.scss']
})
export class InsideElementComponent implements OnInit {
  isAvailable = true;
  constructor() { }

  ngOnInit() {

  }

  myClickFunction(event) {
    // just added console.log which will display the event details in browser on click of the button.
    // alert('Button is clicked');
    this.isAvailable = !this.isAvailable;
    console.log(event.target.value);
 }
}
