import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IntegrationService } from '../services/integration.service';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.scss']
})
export class FormTemplateComponent implements OnInit {
  todaydate;
  componentproperty;
  emailid;
  formdata;
  constructor(private integrationService: IntegrationService) { }

  ngOnInit() {
    this.todaydate = this.integrationService.showTodayDate();
    this.formdata = new FormGroup({
      emailid: new FormControl('angular@gmail.com'),
      passwd: new FormControl('abcd1234')
    });
    this.formdata = new FormGroup(
      {
        emailid: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*')
        ])),
        passwd: new FormControl('', this.passwordvalidation)
      }
   );
  }

  onClickSubmit(data) {
    this.emailid = data.emailid;
    console.log(data);
    // alert('Entered Email id : ' + data.emailid);
  }

  passwordvalidation(formcontrol) {
      if (formcontrol.value.length < 5) {
        return { passwd : true};
      }
  }

}
