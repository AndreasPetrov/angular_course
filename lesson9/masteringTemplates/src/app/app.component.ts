import { Component, OnInit } from '@angular/core';
import { IntegrationService } from './services/integration.service';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'masteringTemplates';
  todaydate;
  constructor(private integrationService: IntegrationService
            , private http: Http) {  }

  ngOnInit() {
      this.http.get('http://jsonplaceholder.typicode.com/users')
               .pipe(map((response) => response.json()))
               .subscribe((data) => console.log(data));

      this.todaydate = this.integrationService.showTodayDate();
  }
}
