import { Component, OnInit, OnChanges, DoCheck, AfterViewChecked } from '@angular/core';
import { AfterContentInit, AfterContentChecked, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-lifetime',
  templateUrl: './lifetime.component.html',
  styleUrls: ['./lifetime.component.scss']
})

export class LifetimeComponent implements OnInit, OnChanges,
             DoCheck, AfterContentInit, AfterContentChecked,
             AfterViewInit, AfterViewChecked, OnDestroy {

  counterInitializer = 0.0;
  ctorCounter: number;
  onChangesCounter: number;
  onInitCounter: number;
  doCheckCounter: number;
  afterContentInitCounter: number;
  afterContentCheckedCounter: number;
  afterViewInitCounter: number;
  afterViewCheckedCounter: number;
  onDestroyCounter: number;

  ctorMessage: string;
  onChangesMessage: string;
  onInitMessage: string;
  doCheckMessage: string;
  afterContentInitMessage: string;
  afterContentCheckedMessage: string;
  afterViewInitMessage: string;
  afterViewCheckedMessage: string;
  onDestroyMessage: string;

  constructor() {
    const ctorCounter = this.counterInitializer++;
    this.ctorMessage = 'Constructor: ' + ctorCounter + ' - before begin';
  }

  ngOnChanges() {
    const onChangesCounter = this.counterInitializer++;
    this.onChangesMessage = 'OnChanges: ' + onChangesCounter + ' - before begin';
  }

  ngOnInit() {
    const onInitCounter = this.counterInitializer++;
    this.onInitMessage = 'onInit: ' + onInitCounter + ' - before begin';
  }

  ngDoCheck() {
    const doCheckCounter = this.counterInitializer++;
    this.doCheckMessage = 'ngDoCheck: ' + doCheckCounter + '  - after begin';
  }

  ngContentInit() {
    const afterContentInitCounter = this.counterInitializer++;
    this.afterContentInitMessage = 'afterContentInit: ' + afterContentInitCounter + ' - after begin';
  }

  ngAfterContentInit() {
   const afterContentInitCounter = this.counterInitializer++;
   this.afterContentInitMessage = 'afterContentInit: ' + afterContentInitCounter + ' - It initializes the component content';
  }

  ngAfterContentChecked() {
    const afterContentCheckedCounter = this.counterInitializer++;
    this.afterContentCheckedMessage = 'afterContentChecked: ' + afterContentCheckedCounter +
    ' - It checks the binding of the external content';
  }

  ngAfterViewInit() {
    const afterViewInitCounter = this.counterInitializer++;
    this.afterViewInitMessage = 'afterViewInit: ' + afterViewInitCounter + '-  It creates the component view';
  }

  ngAfterViewChecked() {
    const afterViewCheckedCounter = this.counterInitializer++;
    this.afterViewCheckedMessage = 'afterViewChecked: ' + afterViewCheckedCounter + ' - It checks the bindings of the component’s view';
  }

  ngOnDestroy() {
    const onDestroyCounter = this.counterInitializer++;
    this.onDestroyMessage = 'onDestroy: ' + onDestroyCounter + ' - after end';
  }
}
