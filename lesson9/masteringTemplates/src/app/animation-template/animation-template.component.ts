import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-animation-template',
  templateUrl: './animation-template.component.html',
  styleUrls: ['./animation-template.component.scss'],
  styles: [`
     div {
        margin: 0 auto;
        text-align: center;
        width:200px;
     }
     .rotate{
        width:100px;
        height:100px;
        border:solid 1px red;
     }
  `],

  animations: [
    trigger('myanimation', [
        state('smaller',
          style({
            transform : 'translateY(100px)'
        }))
        , state('larger',
          style({
            transform : 'translateY(0px)'
        }))
        , transition('smaller => larger', animate('300ms ease-in'))
    ])
  ]
})

export class AnimationTemplateComponent implements OnInit {
  state = 'smaller';
  constructor() { }

  animate() {
    this.state = this.state === 'larger' ? 'smaller' : 'larger';
  }

  ngOnInit() {

  }

}
