import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styles: [`
    .online {
      color: lightgreen
    }
  `]
  // styleUrls: ['./server.component.scss']
})

export class ServerComponent implements OnInit {
  allowNewServer: Boolean = false;
  serverId: number = 10;
  serverStatus: string = 'offline';

  constructor() {
    this.serverStatus = Math.random() > 5 ? 'online' : 'offline';
   }

  ngOnInit() {
  }

  public getServerStatus() {
    return Math.random() > 5 ? 'online' : 'offline';
  }
}
