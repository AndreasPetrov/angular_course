import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding-template',
  templateUrl: './binding-template.component.html',
  styleUrls: ['./binding-template.component.scss']
})

export class BindingTemplateComponent implements OnInit {
  serverCreated = false;
  allowNewServer = false;
  serverCreationStatus = 'No server was createe!';
  serverName = '';

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
   }

  ngOnInit() {
  }

  onCreateServer() {
    this.serverCreated = !this.serverCreated;
    this.serverCreationStatus = 'Server was created: ' + this.serverName;
  }

  onUpdateServerName(event: Event) {
     this.serverName = (<HTMLInputElement>event.target).value;
  }
}
