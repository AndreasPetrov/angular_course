import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingTemplateComponent } from './binding-template.component';

describe('BindingTemplateComponent', () => {
  let component: BindingTemplateComponent;
  let fixture: ComponentFixture<BindingTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
