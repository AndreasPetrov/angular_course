import { MatMenuModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule, MatButtonModule, MatSidenavModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule} from '@angular/router';
import { InsideElementComponent } from './inside-element/inside-element.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ChangeTextDirective } from './directives/change-text.directive';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpProcessingComponent } from './http-processing/http-processing.component';
import { FormTemplateComponent } from './form-template/form-template.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnimationTemplateComponent } from './animation-template/animation-template.component';
import { MaterialTabComponent } from './material-tab/material-tab.component';
import { LifetimeComponent } from './lifetime/lifetime.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { BindingTemplateComponent } from './binding-template/binding-template.component';
import { ServerComponent } from './server/server.component';
import { NgModule } from '@angular/core';
import { MatrixComponent } from './matrix/matrix.component';

@NgModule({
  declarations: [
    AppComponent,
    InsideElementComponent,
    CalendarComponent,
    ChangeTextDirective,
    HttpProcessingComponent,
    FormTemplateComponent,
    AnimationTemplateComponent,
    MaterialTabComponent,
    LifetimeComponent,
    MenuListComponent,
    BindingTemplateComponent,
    ServerComponent,
    MatrixComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    MatMenuModule,
    MatInputModule,
    MatButtonModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatNativeDateModule,
    RouterModule.forRoot([
      {
         path: 'httpProcessing',
         component: HttpProcessingComponent
      },
      {
        path: 'calendar',
        component: CalendarComponent
      },
      {
         path: 'inside',
         component: InsideElementComponent
      },
      {
         path: 'formTemplate',
         component: FormTemplateComponent
      },
      {
        path: 'animation',
        component: AnimationTemplateComponent
     },
      {
        path: 'material',
        component: MaterialTabComponent
     },
      {
        path: 'lifetime',
        component: LifetimeComponent
     },
      {
        path: 'bindingTemplate',
        component: BindingTemplateComponent
      },
      {
        path: 'matrix',
        component: MatrixComponent
     }
   ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
