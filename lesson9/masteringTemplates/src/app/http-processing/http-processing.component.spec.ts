import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpProcessingComponent } from './http-processing.component';

describe('HttpProcessingComponent', () => {
  let component: HttpProcessingComponent;
  let fixture: ComponentFixture<HttpProcessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HttpProcessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
