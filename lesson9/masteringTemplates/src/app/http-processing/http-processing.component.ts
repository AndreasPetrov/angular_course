import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map} from 'rxjs/operators';

@Component({
  selector: 'app-http-processing',
  templateUrl: './http-processing.component.html',
  styleUrls: ['./http-processing.component.scss']
})

export class HttpProcessingComponent implements OnInit {

  constructor(private http: Http) { }
  httpdata;
  name;
  searchparam = 2;

   ngOnInit() {
     this.http.get('http://jsonplaceholder.typicode.com/users')
    .pipe(map((response) => response.json()))
    .subscribe((data) => console.log(data));
   }

   displaydata(data) {
     this.httpdata = data;
   }

   renderData(event) {
       this.http.get('http://jsonplaceholder.typicode.com/users')
       .pipe(map((response) => response.json()))
       .subscribe((data) => this.displaydata(data));
   }

}
